<?php
class Model_Modelo extends CI_Model
{
	protected $id_modelo;
	protected $Modelo;
	protected $idmarca;

	const TABLE_NAME="modelo";

	function __construct()
	{
			parent::__construct();
	}
	public function getId()
	{
		return $this->id_model;
	}

	public function getModel()
	{
		return $this->Model;
	}

	public function getMarca()
	{
		return $this->idmarca;
	}
	public function init($model,$idmarca)
	{
		$this->Modelo=$model;
		$this->idmarca = $idmarca;
	}
	public function setId($id)
	{
		$this->id_modelo = $id;
	}
	public function save()
	{
		$this->db->insert(static::TABLE_NAME, get_object_vars($this)); 
	}
	public function getAll()
	{
		$query= $this->db->get(static::TABLE_NAME);
		return $query->result();
	}
	public function getByMarca($idMarca)
	{
		$sql="select * from ".static::TABLE_NAME.' where idmarca='.$idMarca;
		$query=$this->db->query($sql);
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return NULL;
	}
	public function buscarPorId($id)
	{
		
		$sql = 'select * from '.static::TABLE_NAME.' WHERE id_modelo='.$id; 
		$query= $this->db->query($sql);
		return $query->result();
	}
	
	public function getModelMarca(){
		$sql= "select md.id_modelo,md.Modelo,mr.descripcion from modelo As md inner join marca As mr on md.idmarca = mr.Id_marca";
		$query = $this->db->query($sql);
		if($query->num_rows())
		{
			return $query->result();
		}
		return FALSE;
	}
	public function existenombre($Modelo){
		$data = array($Modelo);
		$sql = 'select * from ' .STATIC::TABLE_NAME. ' WHERE  Modelo=?';
		$query=$this->db->query($sql,$data);
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return null;
	}
	public function eliminar($id)
	{
		$sql='DELETE FROM '.static::TABLE_NAME.' WHERE id_modelo=?';
		$query=$this->db->query($sql,array($id));
	}
	public function buscarPorNombre($nombre)
	{
		$data = array($nombre);
		$sql='SELECT * FROM '.static::TABLE_NAME. ' WHERE Modelo=?';
		$query= $this->db->query($sql,$data);
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return null;
	}
	public function actualizar()
	{
		$valido=$this->buscarPorNombre($this->Modelo);
		if(empty($valido))
		{
			$data = array(
               $this->Modelo,
               $this->idmarca,
				$this->id_modelo
            );
			$sql='update '.static::TABLE_NAME.' SET Modelo=?, idmarca=? WHERE id_modelo=?';
			$response= $this->db->query($sql,$data);
			return 'Marca Actualizada';
		}
		return 'La modelo ya existe';
	}
}		