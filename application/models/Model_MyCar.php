<?php
class Model_MyCar extends CI_Model
{
	protected $id;
	protected $year;
	protected $modelo;
	protected $marca;
	protected $vin;
	protected $precio;
	protected $kilometraje;

	const TABLE_NAME='mycar';

	function __construct()
	{
			parent::__construct();
	}

	public function getYear()
	{
		return $this->year;
	}
	public function getId()
	{
		return $this->id;
	}
	public function getVin()
	{
		return $this->vin;
	}
	public function getModel()
	{
		return $this->model;
	}
	public function getKilometraje()
	{
		return $this->kilometraje;
	}
	public function getPrecio()
	{
		return $this->precio;
	}
	public function init($vin,$model,$marca,$kilometraje,$precio,$year)
	{
		$this->vin=$vin;
		$this->modelo=$model;
		$this->marca=$marca;
		$this->kilometraje=$kilometraje;
		$this->precio=$precio;
		$this->year=$year;
	}
	public function save()
	{
		$this->db->insert(static::TABLE_NAME, get_object_vars($this)); 
	}
	public function listAll()
	{
		$query= $this->db->query('select * from '.static::TABLE_NAME);
		return $query->result();
	}
}