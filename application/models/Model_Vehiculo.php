<?php
class Model_Vehiculo extends CI_Model
{
	protected $id_vehiculo;
	protected $vin;
	protected $idmodelo;
	protected $kilometraje;
	protected $year;
	protected $precio;

	const TABLE_NAME='vehiculo';
	function __construct()
	{
			parent::__construct();
	}
	public function getYear()
	{
		return $this->year;
	}
	public function getId()
	{
		return $this->id_vehiculo;
	}
	public function getVin()
	{
		return $this->vin;
	}
	public function getModel()
	{
		return $this->idmodel;
	}
	public function getKilometraje()
	{
		return $this->kilometraje;
	}
	public function getPrecio()
	{
		return $this->precio;
	}
	public function setId($id)
	{
		$this->id_vehiculo=$id;
	}
	public function init($vin,$idmodel,$kilometraje,$precio,$year)
	{
		$this->vin=$vin;
		$this->idmodelo=$idmodel;
		$this->kilometraje=$kilometraje;
		$this->precio=$precio;
		$this->year=$year;
	}
	public function save()
	{
		$this->db->insert(static::TABLE_NAME, get_object_vars($this));
		$this->id_vehiculo = $this->db->insert_id();
		return $this->id_vehiculo;

	}
	public function getAll()
	{
		$sql='SELECT Id_vehiculo, vin, kilometraje, precio, idmodelo,mo.Modelo,ma.descripcion Marca, year FROM vehiculo ve 
				INNER JOIN modelo mo ON ve.idmodelo=mo.Id_modelo
				INNER JOIN marca ma ON ma.id_marca=mo.idmarca;';

		$query=$this->db->query($sql);
		if($query->num_rows())
		{
			return $query->result();
		}
		return NULL;
	}
	public function getByVin($vin)
	{
		$sql="SELECT * FROM ".static::TABLE_NAME. ' ve 
		INNER JOIN modelo mo ON ve.idmodelo=mo.Id_modelo
		INNER JOIN marca ma ON ma.id_marca=mo.idmarca 
		WHERE vin = "'.$vin.'"';
		$query=$this->db->query($sql);
		if($query->num_rows()>0)
		{
			return $query->row();
		}
		return FALSE;
	}
	public function update($id)
	{
		$this->db->where('id_vehiculo', $id);
		$data=array(
			'vin'=>$this->vin,
			'idmodelo'=>$this->idmodelo,
			'precio'=>$this->precio,
			'year'=>$this->year,
			'kilometraje'=>$this->kilometraje);
		$this->db->update(static::TABLE_NAME, $data); 
	}
	public function delete($id)
	{
		return $this->db->delete(static::TABLE_NAME, array('id_vehiculo' => $id));
	}
	public function getByModelo($idModelo)
	{
		$sql='select * from '.static::TABLE_NAME.' where idmodelo=?';
		$query=$this->db->query($sql,array($idModelo));
		if($query->num_rows())
		{
			return $query->result();
		}
		return NULL;
	}
	public function esIgual(Model_Vehiculo $vehiculo)
	{
		$result=true;
		if($this->id_vehiculo!=$vehiculo->getId())
		{
			return false;
		}
		if($this->getModel()!=$vehiculo->getModel())
		{
			return false;
		}
		if($this->getVin()!=$vehiculo->getVin())
		{
			return false;
		}
		if($this->getYear()!=$vehiculo->getYear())
		{
			return false;
		}
		if($this->getKilometraje()!=$vehiculo->getKilometraje())
		{
			return false;
		}
		if($this->getPrecio()!=$vehiculo->getPrecio())
		{
			return false;
		}
		return $result;
	}
}