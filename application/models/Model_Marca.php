<?php
class Model_Marca extends CI_Model
{
	protected $id_marca;
	protected $descripcion;
	const TABLE_NAME='marca';

	function __construct()
	{
			parent::__construct();
	}

	public function getId()
	{
		return $this->id_marca;
	}
	public function getDescription()
	{
		return $this->descripcion;
	}
	public function setId($id)
	{
		$this->id_marca=$id;
	}
	public function setdescription($descripcion)
	{
		$this->descripcion=$descripcion;
	}

	public function init($descripcion)
	{
		$this->descripcion=$descripcion;
	}
	public function save()
	{
		$this->db->insert(static::TABLE_NAME, get_object_vars($this)); 
	}
	public function buscarPorNombre($nombre)
	{
		$data = array($nombre);
		$sql='SELECT * FROM '.static::TABLE_NAME. ' WHERE descripcion =?';
		$query= $this->db->query($sql,$data);
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		return null;
	}
	public function actualizar()
	{
		$valido=$this->buscarPorNombre($this->descripcion);
		if(empty($valido))
		{
			$data = array(
               $this->descripcion,
               $this->id_marca
            );
			$sql='update '.static::TABLE_NAME.' SET descripcion=? WHERE id_marca=?';
			$response= $this->db->query($sql,$data);
			return 'Marca Actualizada';
		}
		return 'La marca ya existe';

	}
	public function getAll()
	{
		//$query= $this->db->get(static::TABLE_NAME);
		$query= $this->db->query('select * from '.static::TABLE_NAME);
		return $query->result();
	}

	public function buscarPorId($id)
	{
		$query= $this->db->query('select * from '.static::TABLE_NAME.' WHERE id_marca='.$id);
		return $query->result();
	}
	public function eliminarById($id)
	{
		$sql='DELETE FROM '.static::TABLE_NAME.' WHERE id_marca=?';
		$query=$this->db->query($sql,array($id));
	}

}