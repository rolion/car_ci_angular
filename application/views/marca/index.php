<div class="container-fluid text-center">    
  <div class="row content">

  <div class="col-sm-1 sidenav">
  </div>
  
  <div class="col-sm-10 text-center"> 
  	<div class="page-header">
		<h1 class="h1">Marcas</h1>      	
	</div>
	
		<a class="btn btn-default btn-block" href="#/marca/nuevo">Nuevo</a>
		<hr>
		<table class='table table-hover'>
			<thead>
				<th>Id</th>
				<th>Nombre</th>
				<th>Accion</th>
			</thead>
			<tbody>
				<tr ng-repeat="marca in listaMarca ">
					<td>{{marca.id_marca}}</td>
					<td>{{marca.descripcion}}</td>
					<td>
						<a  class="glyphicon glyphicon-pencil" href="#/marca/actualizar/{{marca.id_marca}}"></a>
						<a  class="glyphicon glyphicon-trash" ng-click="eliminar(marca.id_marca ,$index)" href="" ></a>
				</tr>
			</tbody>
		</table>
  </div>
  
  <div class="col-sm-1 sidenav">
  </div>

  </div> <!-- end row content -->
</div> <!-- end container-fluid text-center -->



