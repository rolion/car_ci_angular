<div class="container-fluid text-center">    
  <div class="row content">

  <div class="col-sm-1 sidenav">
    </div>
  
    <div class="col-sm-10 text-left"> 
    	<div class="page-header">
      		<h1 class="h1">Nueva Marca</h1>      	
      	</div>
	  <form name='marcaForm' role='form' class='form-horizontal' novalidate>		
	  	
		<div class="form-group">
	    	<label  for="nombre_marca">Nombre</label>	    	
	    	<input type="text" name="nombre"class="form-control " id="nombre_marca" ng-model="nombre" required>
	    	<p ng-show="marcaForm.nombre.$invalid && marcaForm.nombre.$touched" style="color:red;"
	    	 class="help-block">Debe introducir nombre</p>
	    				
	      		<button type="submint" class="btn btn-default btn-block" ng-click="guardar(marcaForm.$valid)">Guardar</button>
	      		<hr>
	    	
		
		</div>
	</form>
   </div>
  
    <div class="col-sm-1 sidenav">
    </div>

  </div>
</div>

