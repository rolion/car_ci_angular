<!DOCTYPE html>
<html lang="es">
<head >
  <!--Bootstrap-->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <!--AngularJS-->

  
  
</head>

<body >

<div class="container" >
   <h3 ng-show="edit">Registrar Vehiculo</h3>
  <form role="form" class="form-horizontal col-md-offset-3" 
  data-ng-app="myCar" data-ng-controller="Controllerform">
    <div class="form-group">
      <label class="col-sm-2" >Marca:</label>
      <div class="col-sm-4">
        <select name="marcas-angular" class="form-control" 
         ng-model="marca" 
         ng-option="marca.descripcion for marca in marcas"></select>
         <input type="text" value>
      </div>                             
    </div>

    <div class="form-group">
      <label class="col-sm-2">Modelo:</label>
      <div class="col-sm-4">
         <select class="form-control" type="select" name="modelos" ng-model="selectmodelo" ng-disabled="!edit"></select>
      </div>
    </div>

    <div class="form-group">
       <label class="col-sm-2">Año:</label>
       <div class="col-sm-4">
          <input class="form-control" pattern="[0-9]{4}" name="Año"type="text" ng-model="passw1" placeholder="Año" title="Ingresar Año"required>
       </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2">Kilometraje:</label>
      <div class="col-sm-4"> 
         <input class="form-control" pattern="[0-9]{1,5}" name="Kilometraje" type="text" ng-model="passw2" placeholder="Kilometraje" title="Ingrese Kilometraje" required> 
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-2">VIN:</label>
      <div class="col-sm-4">
        <input class="form-control" pattern="[0-9A-Za-z]{5,17}" name="VIN" type="text" ng-model="passw2" placeholder="VIN" title="Ingrese VIN" required>  
      </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2">Precio:</label>
      <div class="col-sm-4">
           <input class="form-control" pattern="[0-9]{1,5}" name="Precio" type="text" ng-model="passw2" placeholder="Precio" title="Se necesita un Precio" required>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-6 col-md-offset-2">
        <button class="btn btn-success"  ng-click="save()"> Registrar</button>
      </div>
    </div>
    </form>
  <script src="<?=base_url('assets/angular.min.js');?>"></script>    
 <script src="<?=base_url('assets/app.js');?>"></script>
</body>
</html>
