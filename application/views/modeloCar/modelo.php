

<div class="container-fluid text-center">    
  <div class="row content">

  <div class="col-sm-1 sidenav">
    </div>
  
    <div class="col-sm-10 text-center"> 
			<form class="form-horizonal" rol="form">
		<div class="page-header">
			 <h1 class="h1">Modelos de vehículos</h1>      	
		</div>
			
				<a class="btn btn-default btn-block" href="#/modelo/addmodelo">NUEVO <span class="badge"></span></a> 
				 <hr>
				<table class="table table-hover" data-ng-show="modelos.length > 0">

					<thead>
						<tr>
							<th style="width:50px">#</th>
							<th>Modelo</th>
							<th>Marca</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						<tr data-ng-repeat="modelo in modelos">
							<td>{{modelo.id_modelo}}</td>
							<td>{{modelo.Modelo}}</td>
							<td>{{modelo.descripcion}}</td>
							<td>
								<a  class="glyphicon glyphicon-pencil" href="#/modelo/actualizar/{{modelo.id_modelo}}"></a>
								<a class="glyphicon glyphicon-trash" ng-click="eliminar(modelo.id_modelo ,$index)" ></a>
							</td>
						</tr>
					</tbody>
				</table>
			
		
		</div>

	</form>     
    </div>
  
  
  </div>
</div>


