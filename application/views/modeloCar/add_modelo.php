<div class="container-fluid text-center">    
  <div class="row content">

  <div class="col-sm-1 sidenav">
    </div>
  
    <div class="col-sm-10 text-left"> 
		<form name="frmaddmodelo" class="form-horizonal" rol="form" novalidate>
		<div class="page-header">
			 <h1 class="h1">Registro de nuevo modelo</h1>      	
		</div>	
					
				<div class="form-group">
					<label>Marca</label><select class="form-control" name="selectmarcas" data-ng-model="selectmarcas" required>
						<option ng-repeat="marc in marcas" value="{{marc.id_marca}}" >{{marc.descripcion}}</option>
				    </select>
				    <p ng-show="frmaddmodelo.selectmarcas.$invalid && frmaddmodelo.selectmarcas.$touched" style="color:red;"
	    	 				class="help-block">Debe introducir nombre</p>
				</div>

				<div class="form-group">
					<label>Nombre</label><input type="text" name="nombre" class="form-control" data-ng-model="nombre" required/>
						<p ng-show="frmaddmodelo.nombre.$invalid && frmaddmodelo.nombre.$touched" style="color:red;"
	    	 				class="help-block">Debe introducir nombre</p>
				</div>

				<div class="form-group">
					<button class="btn btn-default btn-block" data-ng-click="guardar(frmaddmodelo.$valid)">Guardar</button>
				</div>
						
		</div>

	</form>     
    </div>
  
    <div class="col-sm-1 sidenav">
    </div>

  </div>
</div>

