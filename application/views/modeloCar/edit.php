

<div class="container-fluid text-center">    
  <div class="row content">

  
    <div class="col-sm-10 text-center"> 
      
		<form name="fromModeloedit"  class="form-horizonal" rol="form" novalidate>
		
		
			<div class="page-header">
				<h1 class="h1">Edición de modelo</h1>      	
			</div>
					
			
				<div class="form-group">
					<label>Marca</label><select class="form-control" name="selectmarcas" data-ng-model="selectmarcas" required >
						<option  data-ng-selected="true" data-ng-repeat="marc in marcas" value="{{marc.id_marca}}" >{{marc.descripcion}}</option>
				    </select>
				    <p data-ng-show="fromModeloedit.selectmarcas.$invalid && fromModeloedit.selectmarcas.$touched" style="color:red;"
	    	 		class="help-block">Seleccionar Marca</p>
				</div>
				<div class="form-group">
					<label>Nombre</label>
					<input type="text" name="nombre" class="form-control " id="nombre_modelo" data-ng-model="nombre" required/>
					<p data-ng-show="fromModeloedit.nombre.$invalid && fromModeloedit.nombre.$touched" style="color:red;"
	    	 		class="help-block">Debe introducir el nombre</p>
				</div>

				<div class="form-group">
					<button type="submint" class="btn btn-default btn-block" data-ng-click="actualizar(fromModeloedit.$valid)">Guardar</button>
				</div>
			
		
		</form>  
		</div>

   

  

  </div>
</div>





