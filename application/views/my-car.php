<!DOCTYPE html>
<html lang="es">
<head>
	<title>My Car</title>
  	<link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.min.css');?>">
  	<link rel="stylesheet" href="<?=base_url('assets/css/bootstrap-theme.min.css');?>">
</head>	
<body>
	<div class='conteiner'>
	
		<form rol="form" class="form-horizontal col-md-6 col-md-offset-3 ">
		
		<h3>Registrar Vehiculo</h3>
			<div class="col-md-12" data-ng-app="myCar" data-ng-controller="saveCar">
				<h3 ng-show="edit">Registrar Vehiculo</h3>
				
				vin<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" name="title" ng-model="vin" placeholder="Ingrese el # de chasis" required="">
				
				marca<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" name="title" ng-model="marca" placeholder="Ingrese la marca" required="">

				modelo<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" name="title" ng-model="modelo" placeholder="Ingrese el modelo" required="">


				kilometraje<input type="numeric" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" name="title" ng-model="kilometraje" placeholder="Ingrese aqui el kilometraje" required="">

				precio<input type="numeric" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" name="title" ng-model="precio" placeholder="Ingrese el precio" required="">

				year<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" name="title" ng-model="year" placeholder="Ingrese el año del vehiculo" required="">

				<input class="btn btn-success " type="button" ng-click="saveCar()" value='Guardar'>
				
				<table class='table table-hover'>
					<thead>
						<th>vin</th>
						<th>marca</th>
						<th>modelo</th>
						<th>año</th>
						<th>kilometraje</th>
						<th>precio</th>
					</thead>
					<tbody>
						<tr ng-repeat="car in listaCarros">
							<td>{{car.vin}}</td>
							<td>{{car.marca}}</td>
							<td>{{car.modelo}}</td>
							<td>{{car.year}}</td>
							<td>{{car.kilometraje}}</td>
							<td>{{car.precio}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</form>
	</div>
	<script src="<?=base_url('assets/js/angular.min.js');?>"></script>
	<script src="<?=base_url('assets/js/jquery.min.js');?>"></script>
	<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>    
 	<script src="<?=base_url('assets/angular-controller/app.js');?>"></script>

</body>
</html>