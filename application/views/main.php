<!DOCTYPE html>
<html lang="es">
<head>
	<title>My Car</title>
	<link rel="stylesheet" 
          href="<?=base_url('assets/components/bootstrap/dist/css/bootstrap.min.css');?>">
      <link rel="stylesheet" 
          href="<?=base_url('assets/components/bootstrap/dist/css/bootstrap-theme.min.css');?>">
  	<link rel="stylesheet" href="<?=base_url('assets/css/style.css');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>	

<body>
	<script type="text/javascript">
		var baseUrl="<?=base_url()?>";
	</script>

	<div data-ng-app='testRoute'>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">

		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="	#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>

		      	<a class="navbar-brand" href="#"><img src="<?=base_url('assets/img/car-logos-02.png');?>"></a>
		     
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        	<li role="presentation" class="active1"><a href="#/home">Inicio</a></li>
					<li role="presentation"><a href="#/about">Acerca</a></li>
					<li role="presentation"><a href="#/marcas">Marcas</a></li>
					<li role="presentation"><a href="#/modelo">Modelos</a></li>
					<li role="presentation"><a href="#/vehiculo/index">Vehiculo</a></li>

		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	<div class="container" data-ng-view>
	</div>
	</div>

	<!-- DO NOT TOUCH-->
	<script src="<?=base_url('/assets/components/angular/angular.min.js');?>"></script>
	<script src="<?=base_url('assets/components/angular-route/angular-route.min.js');?>"></script>
	<script src="<?=base_url('assets/components/jquery/dist/jquery.min.js');?>"></script>
	<script src="<?=base_url('assets/components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
	<script src="<?=base_url('assets/components/bootbox/bootbox.js');?>"></script> 
	<script src="<?=base_url('assets/components/ngBootbox/ngBootbox.js');?>"></script>    
	<!-- DO NOT TOUCH-->
	
	<!-- ANGULARJS CONTROLLERS -->
	<script src="<?=base_url('assets/angular-controller/TestController.js');?>"></script>
 	<script src="<?=base_url('assets/angular-controller/marcacontroller.js');?>"></script>
 	<script src="<?=base_url('assets/angular-controller/modelocontroller.js');?>"></script>
 	<script src="<?=base_url('assets/angular-controller/Car_Controller.js');?>"></script>
 	<script src="<?=base_url('assets/angular-controller/editmodelocontroller.js');?>"></script>
 	<script src="<?=base_url('assets/angular-controller/addmodelocontroller.js');?>"></script>
	<!-- ANGULARJS CONTROLLERS -->
</body>
<footer class="footer">
<div class="container-fluid text-center">
	<p class="text-muted">
		Barrio equipetrol norte entre 3er y 4to Anillo, calle I #99 
		Telf. (591) 321-6473 - info@tcopfitnescenter.com Santa Cruz, Bolivia - 
		copyright © Todos los derechos reservados
	</p>
</div>
	
</footer>
</html>