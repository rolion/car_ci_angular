<div class="container">

	<form class="form-horizonal" rol="form" novalidate>
		<div class="page-header">
			 <h1>Registro de nuevo Modelo</h1> 
		</div>
		<div class="form-group col-md-6" >
					
			<div class="col-md-12">
				<div class="form-group">
					<label>Marca</label><select class="form-control" data-ng-model="selectmarcas" required>

						<option ng-repeat="marc in marcas" value="{{marc.Id_marca}}" >{{marc.descripcion}}</option>
				    </select>
				</div>
				<div class="form-group">
					<label>Nombre</label><input type="text"  class="form-control" data-ng-model="nombre" required/>
				</div>

				<div class="form-group">
					<button class="btn btn-success" data-ng-click="guardar(add_modelo.$valid)">Guardar</button>
				</div>
			</div> 	
		</div>
		</div>

	</form>
</div> 