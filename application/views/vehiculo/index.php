

<div class="container-fluid text-center">    
  <div class="row content">

  <div class="col-sm-1 sidenav">
    </div>
  
    <div class="col-sm-10 text-center"> 

      		<div class="page-header">
      			<h1 class="h1">Vehículo</h1>      	
      		</div>
				<a class="btn btn-default btn-block" href="#/vehiculo/nuevo">Nuevo</a>
				<hr>
				<table class='table table-hover'>
					<thead>
						<th>Id</th>
						<th>Vin</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Año</th>
						<th>Kilometraje</th>
						<th>Precio</th>
						<th>Accion</th>
					</thead>
					<tbody>
						<tr ng-repeat="vehiculo in listaVehiculos ">
							<td>{{vehiculo.Id_vehiculo}}</td>
							<td>{{vehiculo.vin}}</td>
							<td>{{vehiculo.Marca}}</td>
							<td>{{vehiculo.Modelo}}</td>
							<td>{{vehiculo.year}}</td>
							<td>{{vehiculo.kilometraje}}</td>
							<td>{{vehiculo.precio}}</td>
							<td>
								<a  class="glyphicon glyphicon-pencil" href="#/vehiculo/editar/{{vehiculo.vin}}"></a>
								<a  class="glyphicon glyphicon-trash" ng-click="eliminar(vehiculo.Id_vehiculo ,$index)" href="" ></a>
						</tr>
					</tbody>
				</table>   
				<hr>  
    	</div>
  
    <div class="col-sm-1 sidenav">
    </div>

  </div>
</div>








