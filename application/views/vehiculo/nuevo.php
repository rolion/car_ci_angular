

<div class="container-fluid text-center">    
  <div class="row content">

  
    <div class="col-sm-10 text-left"> 
      	<form role='form' name='nuevoForm'  novalidate>

		
			<div class="page-header">
				<h1 class="h1">Nuevo Vehículo</h1>      	
			</div>

			Vin<input type="text" class="form-control" id="vin_id" name='vin' ng-model="vin"  required/>
			<p ng-show="nuevoForm.vin.$invalid && nuevoForm.vin.$touched" style="color:red;"
		 	class="help-block">Debe introducir el # de chasis</p>
			Marca
			<select name="marca" class="form-control" ng-model="selectedMarca"  ng-change="listarModelos()" required>
			      <option ng-repeat="marca in listamarcas" value="{{marca.id_marca}}">{{marca.descripcion}}</option>
			</select>
			<p ng-show="nuevoForm.marca.$invalid && nuevoForm.marca.$touched" style="color:red;"
		 	class="help-block">Debe seleccionar una marca</p>

			Modelo
			<select name="modelo" class="form-control" ng-model="selectedModelo" required>
			      <option ng-repeat="modelo in listaModelos" value="{{modelo.id_modelo}}">{{modelo.Modelo}}</option>
			</select>
			<p ng-show="nuevoForm.modelo.$invalid && nuevoForm.modelo.$touched" style="color:red;"
		 	class="help-block">Debe seleccionar un modelo</p>
			
			kilometraje<input type="numeric" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" 
			name="kilometraje" ng-model="kilometraje" placeholder="Ingrese aqui el kilometraje" required>
			<p ng-show="nuevoForm.kilometraje.$invalid && nuevoForm.kilometraje.$touched" style="color:red;"
		 	class="help-block">Debe introducir el kilometraje</p>
			
			precio<input type="numeric" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" 
			name="precio" ng-model="precio" placeholder="Ingrese el precio" required>
			<p ng-show="nuevoForm.precio.$invalid && nuevoForm.precio.$touched" style="color:red;"
		 	class="help-block">Debe introducir el precio</p>

			year<input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched"
			name="year" ng-model="year" placeholder="Ingrese el año del vehiculo" required>
			<p ng-show="nuevoForm.year.$invalid && nuevoForm.year.$touched" style="color:red;"
		 	class="help-block">Debe introducir el año del carro</p>
			<hr>
			<input class="btn btn-default btn-block " type="button" ng-click="guardar(nuevoForm.$valid)" value='Guardar'>
			<hr>
		
	</form>
  


	</div>
  </div>
</div>


