<!DOCTYPE html>
<html>
<head>
	<title>My Car</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
	<div class='container' data-ng-app="myCar" data-ng-controller="saveCar" style='margin:0 auto'>
		<div class='row'>
			<div class='row'>
				<div class='col-md-1'>
						<p>vin</p/> 
				</div>
				<div class='col-md-1'>
					<input ng-model="vin"><br/>
				</div>

			</div>
			<div class='row'>
				<div class='col-md-1'>
				<p>marca</p>
			</div>
			</div>
				
			<div class='col-md-1'>
				 <input ng-model="marca"><br/>
			</div>
			<div class='col-md-1'>
				<p>modelo </p>
			</div>
			<div class='col-md-1'>
				<input ng-model="modelo"><br/>
			</div>
			<div class='col-md-1'>
			</div>
			<div class='col-md-1'>
				<p>kilometraje</p>
			</div>
			<div class='col-md-1'>
				<input type="numeric" ng-model="kilometraje"><br/>
			</div>
			<div class='col-md-1'>
				<p>precio</p>
			</div>
			<div class='col-md-1'>
				<input type="numeric" ng-model="precio"><br/>
			</div>
			<div class='col-md-1'>
				<p>
					year
				</p>
			</div>    
			<div>
				 <input type="numeric" ng-model="year"><br/>
			</div>    
		<input type="button" ng-click="saveCar()" value="Guardar">
		</div>
		

		<table>
			<thead>
				<th>vin</th>
				<th>marca</th>
				<th>modelo</th>
				<th>año</th>
				<th>kilometraje</th>
				<th>precio</th>
			</thead>
			<tbody>
				<tr ng-repeat="car in listaCarros">
					<td>{{car.vin}}</td>
					<td>{{car.marca}}</td>
					<td>{{car.modelo}}</td>
					<td>{{car.year}}</td>
					<td>{{car.kilometraje}}</td>
					<td>{{car.precio}}</td>
				</tr>
			</tbody>
		</table>

	</div>
	<script src="<?=base_url('assets/angular.min.js');?>"></script>    
 	<script src="<?=base_url('assets/app.js');?>"></script>
</body>
</html>