<?php

class Model_Marca_Test extends TestCase {
	
	private $_marca;

	public function setUp(){
		$this->resetInstance();
		$this->CI->load->model('Model_Marca');  
        $this->_marca = $this->CI->Model_Marca; 

	}

	public function test_ShouldLoadMarcaClass() {  
        $this->assertNotNull($this->_marca);  
    }

    public function test_ShouldWorkGetters() {  
        $this->_marca->setId(7);  
        
        $this->_marca->setdescription("descripcion1");  
          
        $this->assertEquals(7,$this->_marca->getId());  
        $this->assertEquals("descripcion1", $this->_marca->getDescription());  
         
    }    
	
}