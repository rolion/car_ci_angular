<?php

/**
 * Created by IntelliJ IDEA.
 * User: LionPC
 * Date: 7/14/2016
 * Time: 8:39 PM
 */
class Test_Model_Vehiculo extends TestCase
{
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('Model_Vehiculo');
        $this->obj = $this->CI->Model_Vehiculo;
    }
    public function test_insert_vehiculo()
    {
        $this->obj->init('wearetr123xcv',1,'10000','20000','2010');
        $this->obj->save();
        $actual=$this->obj->getId()==null?FALSE:TRUE;
        $expected=TRUE;
        $this->assertEquals($actual, $expected);
        return $this->obj->getVin();
    }
    /**
     * @depends test_insert_vehiculo
     */
    public function test_get_inserted_vehiculo($vin)
    {
        $result=$this->obj->getByVin($vin);
        $actual = $result!==FALSE?TRUE:false;
        $expected = true;
        $this->assertEquals($actual, $expected);
        return $result->id_vehiculo;
    }
    /**
     * @depends test_get_inserted_vehiculo
     */
    public function test_delete_vehiculo($id)
    {
        $actual=$this->obj->delete($id);
        $expected=TRUE;
        $this->assertEquals($actual, $expected);
    }
    public function test_get_by_vin_false()
    {
        $actual = $this->obj->getByVin('toyota');
        $expected = false;
        $this->assertEquals($actual, $expected);
    }
}