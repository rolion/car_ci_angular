<?php

class Modelo_Test extends TestCase
{
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('Model_Modelo');
        $this->obj = $this->CI->Model_Modelo;
        
    }

    public function test_lista()
    {
        $list = $this->obj->getAll();
        $count = count($list);
        $this->assertEquals($count,1);
    }

    public function test_existemodelo(){

        $list = $this->obj->getAll();
        $sw = false;
        foreach ($list as $item) {
            if(strcmp($item->Modelo,'Corola') == 0){
                 $sw = true;
            }
        }
        $this->assertEquals($sw,true);
        
    }
     public function test_existeModeloMarca(){

        $idmarca = $this->obj->buscarPorId(1);
        $this->assertEquals($idmarca->idmarca,2);
    }
}
