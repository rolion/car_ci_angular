<?php


class Test_Model_Modelo extends TestCase{
	
	public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('Model_Modelo');              //modelo
        $this->obj = $this->CI->Model_Modelo;              //var global
    }

    
    public function test_existe_modelo()
    {
    	$resultado=$this->obj->existenombre('Jazz')==null?false:true;;
    	$esperado=true;
    	$this->assertEquals($resultado, $esperado);

    }

    public function test_existe_modelo1()
    {
    	$resultado=$this->obj->existenombre('patito')==null?false:true;
    	$esperado=false;
    	$this->assertEquals($resultado, $esperado);

    }
   
    
    public function test_buscar_Nombre()
    {
    	$resultado=$this->obj->buscarPorNombre('Jazz')==null?false:true;;
    	$esperado=true;
    	$this->assertEquals($resultado,$esperado);
    }

    public function test_buscar_Nombre1()
    {
    	$resultado=$this->obj->buscarPorNombre('camaro');
    	$esperado=null;
    	$this->assertEquals($resultado,$esperado);
    }
    



 

}