<?php 
class VehiculoController extends CI_Controller
{
	public 	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Model_Modelo');
		$this->load->model('Model_Marca');
		$this->load->model('Model_Vehiculo');
		$this->load->model('Model_MyCar');
	}
	public function viewIndex()
	{
		$this->load->view('vehiculo/index.php');
	}
	public function viewNuevo()
	{
		$this->load->view('vehiculo/nuevo.php');
	}
	public function viewEditar()
	{
		$this->load->view('vehiculo/editar.php');
	}
	public function listarVehiculos()
	{
		$model=new Model_Vehiculo();
		$result=$model->getAll();
		echo json_encode($result);
		exit;
	}
	public function guardar()
	{
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$model=new Model_Vehiculo();
		$model->init($data->vin,$data->modelo,$data->kilometraje,$data->precio,$data->year);
		$mensaje;
		if($model->getByVin($data->vin)===FALSE)
		{
			$model->save();
			$mensaje="guardado";
		}else
		{
			$mensaje="El vehiculo ya existe";
		}
		echo json_encode($mensaje);
		exit;
	}
	public function actualizar()
	{
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$model=new Model_Vehiculo();
		$model->init($data->vin,$data->modelo,$data->kilometraje,$data->precio,$data->year);
		$model->update($data->id);
		echo json_encode("actualizado");
		exit;
	}
	public function obtener($vin)
	{
		$messaje="";
		if(!empty($vin))
		{
			$model=new Model_Vehiculo();
			$response=$model->getByVin($vin);
			echo json_encode($response);
			exit;
		}else
		{
			$messaje="Error";
		}
		echo json_encode($messaje);
		exit;
	}
	public function eliminar($id){
		$model=new Model_Vehiculo();
		$model->delete($id);
		echo json_encode('eliminado');
		exit;
	}
}