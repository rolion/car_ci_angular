<?php 
class save extends CI_Controller
{
	public 	function __construct()
	{
		header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Model_Modelo');
		$this->load->model('Model_Marca');
		$this->load->model('Model_Vehiculo');
		$this->load->model('Model_MyCar');
	}
	public function saveMyCar()
	{
		$data= file_get_contents("php://input");
		$model=new Model_MyCar();
		$model->init($data->vin,$data->idmodel,
			$data->kilometraje,
			$data->precio,
			$data->year);
		$model->save();
		echo 'save';
		exit;
	}
	
}
