<?php
class MainController extends CI_Controller
{
	public 	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->view('main.php');
	}
	public function main()
	{
		$this->load->view('index.html');
	}
	public function home()
	{
		$this->load->view('home.php');
	}
	public function about()
	{
		$this->load->view('about.php');
	}
	public function marca()
	{
		$this->load->view('marca.php');
	}
	public function listarmarca()
	{
		$this->load->view('marca/index.php');
	}
	public function actulizarmarca()
	{
		$this->load->view('marca/editar.php');
	}
	public function nuevaMarca()
	{
		$this->load->view('marca/nuevo.php');	
	}
	public function addmodelo()
	{
		$this->load->view('modeloCar/add_modelo.php');
	}
	public function modelo()
	{
		$this->load->view('modeloCar/modelo.php');
	}
	public function actualizarmodelo()
	{
		$this->load->view('modeloCar/edit.php');
	}
}