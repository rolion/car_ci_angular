<?php 
class CarController extends CI_Controller
{
	public 	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Model_Modelo');
		$this->load->model('Model_Marca');
		$this->load->model('Model_Vehiculo');
		$this->load->model('Model_MyCar');
	}
	

	public function MyCar()
	{
		$this->load->view('my-car.php');
	}
	public function index()
	{
		$this->load->view('main.php');
	}

	public function getModels($idMarca)
	{
		if(empty($idMarca)){
			echo 'id Marca llego vacio';
		}else{
			echo $idMarca;
		}
		$model=new Model_Modelo();
		$modelos=$model->getByMarca($idMarca);
		echo json_encode($modelos);
		exit;
	}
	public function getMarcas()
	{
		$model=new Model_Marca();
		$result=$model->getAll();
		//echo $result;
		echo json_encode($result);
		exit;
	}

	public function listMyCar()
	{
		$model=new Model_MyCar();
		echo json_encode($model->listAll());
	}
	public function saveMyCar()
	{
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$model=new Model_MyCar();
		$model->init($data->vin,$data->modelo,$data->marca,
			$data->kilometraje,
			$data->precio,
			$data->year);
		$model->save();
		echo json_encode($data);
		exit;
	}
	public function saveCar()
	{
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$model=new Model_Vehiculo();
		$result=$model->getByVin($data->car->vin);
		if($result)
		{
			echo 'El vehiculo ya existe';
			exit;
		}
		if($data->precio<=0)
		{
			echo 'Debe introducir precio';
			exit;
		}
		if($data->kilometraje<=0)
		{
			echo 'Debe introducir Kilometraje';
			exit;
		}
		$model->init($data->car->vin,$data->car->idmodel,$data->car->kilometraje,$data->car->precio);
		$model->save();
	}
	public function eliminar($id){
		$model=new Model_Vehiculo();
		$model->delete($id);
		echo json_encode('eliminado');
		exit;
	}
}