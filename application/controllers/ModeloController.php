<?php 
class ModeloController extends CI_Controller
{
	public 	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Model_Modelo');
		$this->load->model('Model_Vehiculo');
	}


	public function listar()
	{
		$model = new Model_Modelo();
		$result = $model -> getAll();
		echo json_encode($result);
		exit;
	}

	public function listarVista()
	{
		$model = new Model_Modelo();
		$result = $model -> getModelMarca();
		echo json_encode($result);
		exit;
	}

	public function guardar()
	{
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$modelo =new Model_Modelo();
		$namemodel = $modelo->existenombre($data->Modelo);
		$message;
		if(empty($namemodel)){
			$modelo->init($data->Modelo,$data->idmarca);
			$modelo->save();
			$message="Modelo Guardado";
		}else{
			$message = "La marca ya existe";
		}
		

		echo json_encode($message);
		exit;
	}

	public function obtener()
	{
		$data = file_get_contents("php://input");
		if(!empty($data)){
			$data = json_decode($data);
			$model = new Model_Modelo();
			$result = $model->buscarPorId($data);
			echo json_encode($result);
			exit;
		}
		return json_encode('id no ingresada');
	}

	public function eliminar($id)
	{
		$modelo = new Model_Vehiculo();
		$modelos = $modelo->getByModelo($id);
		$message;
		if(empty($modelos))
		{
			$modelo = new Model_Modelo();
			$modelo->eliminar($id);
			$message='Modelo eliminado';
		}else
		{
			$message = 'El modelo no puede ser eliminado';
		}
		echo json_encode($message);
		exit;
	}
	public function actualizar()
	{
		//TODO
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$model=new Model_Modelo();
		$model->init($data->Modelo,$data->idmarca);
		$model->setId($data->id_modelo);
		$response=$model->actualizar();
		echo json_encode($response);
		exit;
	}

}
