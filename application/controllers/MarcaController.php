<?php 
class MarcaController extends CI_Controller
{
	public 	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Model_Marca');
		$this->load->model('Model_Modelo');
	}
	public function obtener()
	{
		$data= file_get_contents("php://input");
		if(!empty($data)){
			$data=json_decode($data);
			$model=new Model_Marca();
			$result=$model->buscarPorId($data);
			echo json_encode($result);
			exit;
		}
		return json_encode('id not set');
		exit;

	}
	public function guardar()
	{
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$model=new Model_Marca();
		$marca=$model->buscarPorNombre($data->nombre);
		$message;
		if(empty($marca)){
			$model->init($data->nombre);
			$model->save();	
			$message="Marca guardada";
		}else{
			$message="La marca ya existe";
		}
		
		echo json_encode($message);
		exit;
	}
	public function listar()
	{
		$model=new Model_Marca();
		$result=$model->getAll();
		echo json_encode($result);
		exit;
	}
	public function actualizar()
	{
		//TODO
		$data= file_get_contents("php://input");
		$data=json_decode($data);
		$model=new Model_Marca();
		$model->init($data->descripcion);
		$model->setId($data->id_marca);
		$response=$model->actualizar();
		echo json_encode($response);
		exit;
	}
	public function eliminar($id)
	{
		$modelo=new Model_Modelo();
		$modelos=$modelo->getByMarca($id);
		$message;
		if($modelos===NULL)
		{
			$modelo=new Model_Marca();
			$modelo->eliminarById($id);
			$message='eliminada';
		}else
		{
			$message='no eliminada';
		}
		echo $message;
		exit;
	}
	public function getMarcaModelos($idMarca)
	{
		$model=new Model_Modelo();
		$response=$model->getByMarca($idMarca);
		echo json_encode($response);
		exit;
	}
}
