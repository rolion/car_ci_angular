app.controller('editmodeloCtrl', ['$scope','$http','$routeParams','$ngBootbox',editCtrl]);
function editCtrl($scope,$http,$routeParams,$ngBootbox)
{	$scope.id;
	$scope.marcas;
	
	if(!angular.isUndefined($routeParams.id_modelo))
	{
		$http.post("ModeloController/obtener/",$routeParams.id_modelo)
		.then(function(response)
		{
			$scope.nombre = response.data[0].Modelo;
			$scope.id = response.data[0].id_modelo;
			$http.get("MarcaController/listar").then(function(data){
				$scope.marcas = data.data;
				$scope.selectmarcas = response.data[0].idmarca;
			});
			
		},
		function(data)
		{
			console.log(data.data);
		});
	}

	$scope.actualizar = function(isValido){
		if(isValido){
			$http.post("ModeloController/actualizar",
				{
					id_modelo:$scope.id,
					Modelo:$scope.nombre,
					idmarca:$scope.selectmarcas
				}
			).
			then(function(data){
				$ngBootbox.alert(data.data)
				    .then(function() {
				        console.log('Alert closed');
				    });
			},function(data)
			{
				console.log(data.data);
			});			
		}else
			$ngBootbox.alert('Debe introducir el nombre de la marca');
	};
}