app.controller('vehiculoController',['$scope','$http','$routeParams','$ngBootbox',vehiculo]);

function vehiculo($scope,$http,$routeParams,$ngBootbox)
{
	$scope.Id_vehiculo;
	$scope.vin;
	$scope.idmarca;
	$scope.Marca;
	$scope.idModelo;
	$scope.Modelo;
	$scope.year;
	$scope.kilometraje;
	$scope.precio;
	$scope.listamarcas;
	$scope.listaVehiculos;
	$scope.selectedModelo;
	$scope.selectedMarca;
	$scope.eliminar=function (id,index){

		$ngBootbox.confirm('Estas seguro?')
		    .then(function() {
		    	$http.get("VehiculoController/eliminar/"+id).
		    	then(function(data)
		    		{
		    			console.log(data);
		    			$ngBootbox.alert('se elimino el vehiculo')
				    		.then(function() {
				        	console.log('Alert closed');
				    	});
				    		$scope.listaVehiculos.splice(index,1);
		    		},
		    		function(data)
		    		{
		    			$ngBootbox.alert('no se puedo eliminar el vehiculo')
				    	.then(function() {
				        console.log('Alert closed');
				    	});

		    		});
		        	
		    }, function() {
		        console.log('Confirm dismissed!');
		    });
	}
	$scope.listarModelos=function()
	{
		$http.get("MarcaController/getMarcaModelos/"+$scope.selectedMarca).then(
		function(data)
		{
			$scope.listaModelos=data.data;
			console.log(data);
		},
		function(data)
		{
			console.log(data);
		});
	};
	$scope.guardar=function(valido)
	{
		if(valido)
		{
					$http.post("VehiculoController/guardar",{
						vin:$scope.vin,
						precio:$scope.precio,
						year:$scope.year,
						kilometraje:$scope.kilometraje,
						precio:$scope.precio,
						modelo:$scope.selectedModelo,
					}).then(
					function(data)
					{
						$ngBootbox.alert(data.data)
						    .then(function() {
						        console.log('Alert closed');
						    });
					},
					function(data)
					{	
						console.log(data);
					});
		}else
		{
			$ngBootbox.alert("Debe llenar todo el formulario")
			    .then(function() {
			        console.log('Alert closed');
			    });
		}
	};

	listarMarcas($http,$scope);

	listarVehiculos($http,$scope);
	init($http,$scope,$routeParams);
	$scope.getMarcas=listarMarcas($http,$scope);
	$scope.actualizar=function(valido){
		if(valido)
		{
			$http.post("VehiculoController/actualizar/",{
				vin:$scope.vin,
				precio:$scope.precio,
				year:$scope.year,
				kilometraje:$scope.kilometraje,
				modelo:$scope.selectedModelo,
				id:$scope.Id_vehiculo
			})
			.then(function(response)
			{
				$ngBootbox.alert(response.data)
				    .then(function() {
				        console.log('Alert closed');
				    });
			},
			function(data)
			{
				console.log(data);
			});
		}else
		{
			$ngBootbox.alert("Debe llenar todo el formulario")
			    .then(function() {
				        console.log('Alert closed');
			});
		}
	}
};
function init($http,$scope,$routeParams){
	if(!angular.isUndefined($routeParams.vin))
	{
		$http.get("VehiculoController/obtener/"+$routeParams.vin)
		.then(function(response)
			{
				console.log(response);
				$scope.vin=response.data.vin;
				$scope.precio=response.data.precio;
				$scope.year=response.data.year;
				$scope.kilometraje=response.data.kilometraje;
				$scope.precio=response.data.precio;
				$scope.selectedMarca=response.data.id_marca;
				$scope.listarModelos();
				$scope.selectedModelo=response.data.id_modelo;
				$scope.Id_vehiculo=response.data.id_vehiculo;
				console.log("response="+response.data.id_vehiculo);
			},
			function(data)
			{
				console.log(data);
			});
	}
};
function listarVehiculos($http,$scope)
{
	$http.get("VehiculoController/listarVehiculos").then(
			function(data)
			{
				console.log(data);
				$scope.listaVehiculos=data.data;
			},
			function(data)
			{
				console.log(data);
			});
};
function listModelos($http,$scope)
{
	$http.get("MarcaController/getMarcaModelos/"+$scope.selectedMarca).then(
		function(data)
		{
			$scope.listaModelos=data.data;
		},
		function(data)
		{

		});
};
function listarMarcas($http,$scope)
{
	$http.get("MarcaController/listar").then(
			function(data)
			{
				$scope.listamarcas=data.data;
			},function(data)
			{

			}
		);
};
function actualizarVehiculo($http,$scope,$ngBootbox)
{
		$http.post("VehiculoController/actualizar/",{
			vin:$scope.vin,
			precio:$scope.precio,
			year:$scope.year,
			kilometraje:$scope.kilometraje,
			modelo:$scope.selectedModelo,
		})
		.then(function(response)
			{
				$ngBootbox.alert(data.data)
				    .then(function() {
				        console.log('Alert closed');
				    });
			},
			function(data)
			{
				console.log(data);
			});
};