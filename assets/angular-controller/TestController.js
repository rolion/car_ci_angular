var app=angular.module('testRoute',['ngRoute','ngBootbox']);

app.config(['$routeProvider',function ($routeProvider)
{
	$routeProvider
	.when('/',{
		templateUrl: 'MainController/main'
	})
	.when('/home',{
		templateUrl: 'MainController/home'
	})
	.when('/about',{
		templateUrl: 'MainController/about'
	})
	.when('/marcas',{
		templateUrl: 'MainController/listarmarca',
		controller: 'marca'
	})
	.when('/marca/nuevo',{
		templateUrl: 'MainController/nuevaMarca',
		controller: 'marca'
	})
	.when('/marca/actualizar/:id_marca',{
		templateUrl: 'MainController/actulizarmarca',
		controller: 'marca'
	})
	.when('/modelo',{
		templateUrl: 'MainController/modelo',
		controller: 'modeloCtrl'
	})
	.when('/modelo/actualizar/:id_modelo', {
		templateUrl: 'MainController/actualizarmodelo',
		controller: 'editmodeloCtrl'
	})
	.when('/modelo/addmodelo',{
		templateUrl: 'MainController/addmodelo',
		controller: 'addmodeloCtrl'
	})
	.when('/vehiculo/index',
	{
		templateUrl: 'VehiculoController/viewIndex',
		controller: 'vehiculoController'
	})
	.when('/vehiculo/nuevo',
	{
		templateUrl: 'VehiculoController/viewNuevo',
		controller: 'vehiculoController'
	})
	.when('/vehiculo/editar/:vin',
	{
		templateUrl: 'VehiculoController/viewEditar',
		controller: 'vehiculoController'
	})
	.otherwise({
		redirectTo:'/'
	});
}])