var app=angular.module('myCar',[]);

app.controller('saveCar',['$scope','$http',saveMyCar]);
function saveMyCar($scope,$http)
{
	$scope.marca;
	$scope.modelo;
	$scope.vin;
	$scope.year;
	$scope.precio;
	$scope.kilometraje;
	$scope.listaCarros=[];
	$http.get('listMyCar').success(
		function(data)
		{
			$scope.listaCarros=data;
		}).
		error(function(data){
				alert(data);
		});
	$scope.saveCar=function(){
		$http({
		method:"POST",
		url:  baseUrl+"CarController/saveMyCar",
		 data: {
			        vin:$scope.vin,
			        marca:$scope.marca,
			        modelo:$scope.modelo,
			        year:$scope.year,
			        precio:$scope.precio,
			        kilometraje:$scope.kilometraje,
			    },
		headers:{ 
			'Content-Type': 'application/x-www-form-urlencoded' 
		}
		}).success(function(data)
		{
			$scope.listaCarros.push(data);
			
		}).error(function(data)
		{
			console.log(data);
		});
	}

};

app.controller( 'Controllerform',['$scope','$http',loadCombo]);
function loadCombo ($scope,$http){
	$scope.marcas = [];
	$scope.getMarcas=$http({
 			method: "get",
 			url:  "CarController/getMarcas",
 			headers:{ 
 						'Content-Type': 'application/x-www-form-urlencoded'
 					}
 		}).success(function(data)
		{
 			$scope.marcas =angular.fromJson(data) ;
 			$scope.marca = $scope.marcas[0];

	 	}).error(function(data)
	 	{
	 		alert('error '+data);
	 	});
};


