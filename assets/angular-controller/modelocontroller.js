app.controller('modeloCtrl', ['$scope','$http','$routeParams','$ngBootbox',modelo]);

function modelo($scope,$http,$routeParams,$ngBootbox){
	
	$http.get("ModeloController/listarVista").success(function(data)
	{
			$scope.modelos = data;
	});

	$scope.eliminar=function(idmodelo,index){
		$ngBootbox.confirm('Estas seguro?')
		    .then(function() {

		    	$http.get("ModeloController/eliminar/"+idmodelo).
		    	then(function(data)
		    		{
		    			console.log(data);
		    			$ngBootbox.alert('se elimino el modelo')
				    		.then(function() {
				        	console.log('Alert closed');
				    	});
				    		$scope.modelos.splice(index,1);
		    		},
		    		function(data)
		    		{
		    			$ngBootbox.alert('no se puede eliminar'+idmodelo)
				    	.then(function() {
				        console.log('Alert closed');
				    });

		    		});
		        	
		    }, function() {
		        console.log('Confirm dismissed!');
		    });
	};
}	
