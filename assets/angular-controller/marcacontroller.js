app.controller('marca',['$scope','$http','$routeParams','$ngBootbox',marca]);

function marca($scope,$http,$routeParams,$ngBootbox)
{
	$scope.id;
	$scope.nombre;
	$scope.listaMarca;
	$scope.params=$routeParams;
	if( !angular.isUndefined($routeParams.id_marca))
	{
		$http.post("MarcaController/obtener/",$routeParams.id_marca)
		.then(function(response)
			{
				$scope.nombre=response.data[0].descripcion;
				$scope.id=response.data[0].id_marca;
			},
			function(data)
			{
				console.log(data);
			});
	}
	$scope.getById=function(){
		
	}
	$scope.guardar=function(isValid){
		if(isValid){
			$http.post("MarcaController/guardar",{nombre:$scope.nombre}).
			then(function(data)
			{
				$ngBootbox.alert(data.data)
				    .then(function() {
				        console.log('Alert closed');
				    });
			},function(data)
			{
				console.log(data.data);
			});			
		}else{
			alert('invalido');
		}

	};
	$scope.getMarcas=$http({
			method:"GET",
			url:   baseUrl+"/MarcaController/listar",
			headers:{ 
				'Content-Type': 'application/json' 
			}
			}).success(function(data)
			{
				$scope.listaMarca=data;	
			}).error(function(data)
			{
				console.log('Error al listar las marcas');
			});			
		
	$scope.actualizar=function(esValido){
		if(esValido){
			$http.post
			('MarcaController/actualizar',
				{
					id_marca:$scope.id,
					descripcion:$scope.nombre
				}
			).
			then(function(data){
				console.log(data);
				var message;
				$ngBootbox.alert(data.data)
				    .then(function() {
				        console.log('Alert closed');
				    });
			},
			function(data){
				console.log(data);
			});			
		}else
			alert('Debe introducir el nombre de la marca');
	};

	$scope.listar=function(){
		if(isValid){
			$http({
			method:"GET",
			url:  baseUrl+"/MarcaController/listar",
			headers:{ 
				'Content-Type': 'application/json' 
			}
			}).success(function(data)
			{
				alert('Marca Guardad');
				
			}).error(function(data)
			{
				alert('Error al guardar');
			});			
		}else{
			alert('invalido');
		}
	}
	$scope.eliminar=function(idMarca,index){
		$ngBootbox.confirm('Estas seguro?')
		    .then(function() {
		    	$http.get("MarcaController/eliminar/"+idMarca).
		    	then(function(data)
		    		{
		    			console.log(data);
		    			var message;
		    			if(data.data=="eliminada"){
		    				message="Marca Eliminada";
		    				$scope.listaMarca.splice(index,1);
		    			}else{
		    				message="No se puede eliminar una marca que tiene modelos registrados"
		    			}
		    			$ngBootbox.alert(message)
				    		.then(function() {
				        	console.log('Alert closed');
				    	});
				    		
		    		},
		    		function(data)
		    		{
		    			$ngBootbox.alert('no se puedo eliminar'+idMarca)
				    	.then(function() {
				        console.log('Alert closed');
				    });

		    		});
		        	
		    }, function() {
		        console.log('Confirm dismissed!');
		    });
	};
}