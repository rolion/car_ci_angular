app.controller('addmodeloCtrl', ['$scope','$http','$ngBootbox',addCtrl]);
function addCtrl ($scope,$http,$ngBootbox) {
	$scope.marcas =[];
	$http.get('MarcaController/listar').success(function(data){
		$scope.marcas = data;
	});
	$scope.guardar=function(isValid){
		if(isValid){
			$http.post("ModeloController/guardar",{Modelo:$scope.nombre,idmarca:$scope.selectmarcas}).
			then(function(data)
			{
				$ngBootbox.alert(data.data)
				    .then(function() {
				        console.log('Alert closed');
				    });
			},function(data)
			{
				console.log(data.data);
			});			
		}else{
			$ngBootbox.alert('invalido');
		}

	};
}