CREATE TABLE `mycar` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `vin` text NOT NULL,
 `year` int(11) NOT NULL,
 `kilometraje` double NOT NULL,
 `marca` text NOT NULL,
 `modelo` text NOT NULL,
 `precio` double NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE marca
(
	id_marca INT NOT NULL AUTO_INCREMENT,
	descripcion TEXT,
	PRIMARY KEY (id_marca)

) 
;


CREATE TABLE modelo
(
	id_modelo INT NOT NULL AUTO_INCREMENT,
	Modelo TEXT,
	idmarca INT,
	PRIMARY KEY (Id_modelo),
	INDEX IXFK_Modelo_marca (idmarca ASC)

) 
;


CREATE TABLE vehiculo
(
	id_vehiculo INT NOT NULL AUTO_INCREMENT,
	vin VARCHAR(50),
	kilometraje INT,
	precio DOUBLE,
	idmodelo INT,
	year INT,
	PRIMARY KEY (Id_vehiculo),
	INDEX IXFK_Vehiculo_Modelo (idmodelo ASC)

) 
;





ALTER TABLE modelo ADD CONSTRAINT FK_Modelo_marca 
	FOREIGN KEY (idmarca) REFERENCES marca (id_marca)
;

ALTER TABLE vehiculo ADD CONSTRAINT FK_Vehiculo_Modelo 
	FOREIGN KEY (idmodelo) REFERENCES modelo (id_modelo)
;
